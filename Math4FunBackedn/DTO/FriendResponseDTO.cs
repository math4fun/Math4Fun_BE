﻿using Math4FunBackedn.Entities;

namespace Math4FunBackedn.DTO
{
    public class FriendResponseDTO
    {
        public User User { get; set; }
        public bool IsYourFriend { get; set; }
    }
}
